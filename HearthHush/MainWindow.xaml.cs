﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HearthstoneHushWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
			Reader.Debug(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)+"\\Appdata\\LocalLow\\");
			Reader.Debug(System.IO.Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Appdata\\LocalLow\\"));
			Reader.Debug(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\..\\LocalLow\\");
			Reader.Debug(System.IO.Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\..\\LocalLow"));
		}

        private void StartRead(object sender, RoutedEventArgs e)
        {
            Reader.Read();
        }

        private void EndRead(object sender, RoutedEventArgs e)
        {
            Reader.read = false;
        }

		private void CheckEnemyVibe(object sender, RoutedEventArgs e) {
			Reader.vibrateOnEnemy = true;
		}

		private void UncheckEnemyVibe(object sender, RoutedEventArgs e) {
			Reader.vibrateOnEnemy = false;
		}

		private void CheckFriendlyVibe(object sender, RoutedEventArgs e) {
			Reader.vibrateOnFriendly = true;
		}

		private void UncheckFriendlyVibe(object sender, RoutedEventArgs e) {
			Reader.vibrateOnFriendly = false;
		}

		private void CheckPackVibe(object sender, RoutedEventArgs e) {
			Reader.vibrateOnPack = true;
		}

		private void UncheckPackVibe(object sender, RoutedEventArgs e) {
			Reader.vibrateOnPack = false;
		}

		private void TestConnection(object sender, RoutedEventArgs e) {
			//Reader.Vibrate(5);
		}

		private void StopTest(object sender, RoutedEventArgs e) {
			//Reader.Vibrate(0);
		}
	}
}
